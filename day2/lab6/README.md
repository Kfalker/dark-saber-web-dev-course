# Lab 6 - Poker Hand Generator with React

##### Estimated Time to Complete: 1.5 hours - 2 hours

In `lab6`, we will use the `Poker Hand Calculator` you built in `lab3` for a `ReactJS` application to make it interactive!

Overall, we will learn / re-enforce the following concepts:

1. Bringing in `JavaScript` files to a `ReactJS` project
2. Adding form fields to a `ReactJS` page using Grid
3. React Router
4. Separating ReactJS Components into Separate Files

### Part 0: Start with the Base

To begin, work from the `lab6-base` project in this directory.

To install the packages, simply run `npm install` from the root directory of the project `lab6-base/`

Once you perform the installation of the required libraries, you can now run the app using `npm start`

### Part 1: React Router

The first part of this lab is to create a React Router using the `react-router-dom` library. This will allow you to create different pages. In this lab, we will be creating 3 pages.

1. A Home Page that allows you to get to 2 sub pages.
2. A Day of the Week Page that contains code from the previous `lab5`
3. A 5 Poker Game that runs through a game of poker between 2 plays and displays the winner using code written in `lab6`

You'll want to start by added `react-router-dom` to your `lab6` project. Here's the documentation

https://reactrouter.com/en/main/start/tutorial

Once installed, you'll want to start creating new files

Until now, we've been working with `App.jsx`. Now, we will be moving away from `App.jsx` with 3 new components that reflect the 3 pages we are creating

To start, you'll want to setup a `routes` directory within the `src` directory. Within this, we will have the following files

- `src/routes/root.jsx`
- `src/routes/dow-page.jsx`
- `src/routes/poker-page.jsx`

Each of these files will act as a component that will render one of the pages

The first main objective is to make the `root.jsx` page have `2 buttons`. One button that will navigate to the `Day of the Week` page and another one that will navigate to the `Poker` page.

Below is an example of what the root component might look like

```
export default function Root() {

return (
    <>
      <div className="App">
        <header className="App-header">
          <h1>Your Content</h1>
        </header>
      </div>
    </>
)
}
```

Keep in mind, this example is using the original `App` and `App-header` class names which use `css` properties from the `App.css` file. You can continue to use this or you can create a new style sheet

Within this component, create 2 `MUI` buttons that link to both the `Poker` and `Day of the Week` pages

Note: You'll need to look up how to navigate to different pages

Next, we will setup React Router within the `index.js` file. Add a `router` below and replace the `<App />` with the `router`

```
const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
  }
]);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
```

Next, lets add a few routes. Look up how to add new routes to a router, and add a `Poker` page and a `Day of the Week` page.

Once you have the links, create new components where you display a `<h1>THIS IS THE <BLANK> PAGE </h1>` on each respective page

When you have the buttons working and you are routed to the pages, you've completed this step.

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

### Part 2: Day of the Week

For the next challenge, you'll be porting the code you wrote over from the last `lab5` to the Day of the Week component you made. This should be a simple port. You might need to make modifications to ensure that your app is styled correctly

Once your `lab5` code is ported over, call us over to show us the working Day of the Week page

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

### Part 3: 5 Card Poker Game

Once you have React Router working, it's time to build the poker game!

For this task, you'll be using your code from `lab3` to build an interactive poker game.

The game will allow you to walk through a game of 5 card poker where there are `5 game stages`:

1. `Start of Game`
2. `The Draw` - 3 Cards are pulled from a deck of cards per player
3. `The Turn` - 1 additional card is pulled from the deck of cards per player (each player has 4 cards total)
4. `The River` - 1 additional and the last card is pulled from the deck of cards per player (each play will have 5 cards total)
5. `End of Game` - The game results should be evaluated using the poker hand evaluation code you wrote for `lab3`

This game is meant to be for **2 players** not more

You must have some button that indicates to start the game, iterate through the turns, and refresh the game once a game is completed

At the end of the game, you should display the winner and the combination that won the player the hand

Finally, during the entire game, each plays cards that are drawn should be visible.

#### Approach

For this exercise, you'll need to build a `Deck` object that builds a deck of cards and shuffles it. We've done this for you ahead of time

```
export function Deck() {
  const vals = [
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "T",
    "J",
    "Q",
    "K",
    "A",
  ];
  const suits = ["S", "D", "H", "C"];
  let deck = [];

  function shuffle(array) {
    var m = array.length,
      t,
      i;

    // While there remain elements to shuffle…
    while (m) {
      // Pick a remaining element…
      i = Math.floor(Math.random() * m--);

      // And swap it with the current element.
      t = array[m];
      array[m] = array[i];
      array[i] = t;
    }

    return array;
  }

  for (const suit of suits) {
    for (const val of vals) {
      deck.push(`${val}${suit}`);
    }
  }

  return shuffle(deck);
}

```

To use this, you simply call the function that returns a new shuffled array filled with 52 cards.

```
let deck = Deck()
```

Be creative in this exercise and get started!

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

### Part 4: Pushing to Gitlab

Once you completed all parts of the lab, and you have a verified working solution, you're ready to commit your progress to Git just like you did in previous labs.

Let us know when you've pushed your code to GitLab.

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

#### Congrats! You've completed Lab 6!
