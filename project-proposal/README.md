# Dark Saber Project Proposal

Your task is to design a project proposal that you want to build in React

Requirements

- This project MUST contain a ReactJS Client, an API, and a Database (MongoDB)
- Redux Tool Kit (RTK) - Lab 9, and Docker / docker-compose - Lab 10 must be used regardless if you complete lab 9 or 10.
- Projects proposals must be at least 1 page in length and can not exceed 2 pages long

### Required sections

- Project Overview (Project Vision)
- AF Alignment (How does it align to the Air Force)
- Stakeholders (Who would use the product)
- Minimum Viable Product Requirements (Requirements that you can achieve in a week)
- Vision Requirements (Future requirements but aren’t necessary for an MVP)
- Design (Come up with a Mock Design to show how the application would look) (Powerpoint, Figma, Sketch)

##### Note: Project can be a team project or individual project - Up to YOU

#### Expectations

Project must be something that you intend to continue following the course and will push to building for the USAF.
