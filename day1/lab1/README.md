# Lab 1 - Getting started with NodeJS / JavaScript

##### Estimated Time to Complete: 45 minutes - 1 hour

## Part 0: NodeJS

`NodeJS` provides a server side JavaScript interpreter agnostic from the web browser.
For your first step, you'll need to install `NodeJS` on your machine. (**Classroom Students**: This is already installed for you.)

Once you have `NodeJS` up and running, you'll want to write your first NodeJS program!

Create a file called `part0.js` that logs `Hello World` to the `console` when executed.

Running a NodeJS program is simple: 
```shell
$ node <input_file>
```

Run your new Node program and call us over when you have `Hello World` in your console.

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

## Part 1: Variables and Conditions

In the next part of this lab, we'll test your knowledge of JavaScript basics.

### 1. Create a new file called `part1.js`

Now, let's create 2 variables called `X` and `Y`. Initialize them to a couple of random numbers between 1-100. Add those variables together and save the result in a variable called `SUM`.

Create 3 additional variables: `MULT`, `DIV`, `SUB`. Perform the associated mathematical arithmetic and store the result in those variables.

Next, log all of these variables to the console in a nicely formatted line.

```javascript
// Option A - String Interpolation (preferred)
const res = `Add Result=${SUM}, Subtract Result=${SUB}, ...`;
console.log(res);

// Option B - String Concatenation
const res = "Add Result=" + SUM + ", Subtraction Result=" + SUB + ", ...";
console.log(res);
```

### 2. Write a conditional that determines if `X` is an `Even` or `Odd` number

Print `X is even` or `X is odd`, as applicable.

**Hint**: Use modulus operator - `%`

### 3. Write a conditional that determines if `X` is a factor of `Y`

Print `X is a factor of Y` or `X is not a factor of Y`, as applicable.

**Bonus**: Try to use a ternary expression instead of an `if` statement for both steps 2 & 3.

### 4. Write a multistep conditional statement

Print `X is less than 0`, `X is equal to 0`, or `X is greater than 0`, as applicable.

**Bonus**: Try to create a nested ternary expression instead of an `if` statement for this step.

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

## Part 2: User Input, Functions, and Objects

### 1. Create a new file called `part2.js`

The following NodeJS example demonstrates how to receive input from a `stdin` (your console in this case):
```javascript
const readline = require("node:readline/promises").createInterface({
    input: process.stdin,
    output: process.stdout,
});

readline.question("Who are you?\n").then(name => {
    console.log(`Hey there ${name}!`);
});
```

```shell
$ node read.js
Who are you?
> Roger
Hey there Roger!
$
```

Run the code to try it out: `node ./part2.js`

### 2. Create an ES6 arrow function to return a user's input based on a variable prompt

```javascript
const getInput = async () => {
  // Your code here (hint: You need to return a promise!)
  
  // example
  const result1 = await readline.question("My question");
}
```

The new function, `getInput`, should allow the following code snippet to run:
```javascript
getInput("What is your name?").then(theirName => {
    console.log(`My name is ${theirName}`);
});
```

### 3. Create a function that collects a `firstName`, `lastName`, `age`.

The new function, `getProfile`, should return a JavaScript Object with the following properties:
```javascript
{
    firstName: 'Joe',
    lastName: 'Smith',
    age: '30',
}
```

**Note**: Reuse the `getInput` function you already wrote to accomplish this.
**Note 2**: You'll need to call `readline.close()` just before your script ends in order to release the terminal, otherwise your script may not end without manual intervention with `CTRL+C`.


Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

## Part 3: Arrays, Loops, Map, Filters

### 1. Create a new file called `part3.js`

### 2. Array Challenge. 

Create an array that contains all the details you'd like to capture in a user profile. (i.e. create an array with the following items: `firstName`, `lastName`, `age`, `address`, `phoneNumber`, `favoriteColor`)

Create a new function, `getProfileArray`, that takes this array and prompts the user for an answer to each item with: `What is your ${item}?`. This function should return an object with the answer to each prompt like below:
```
{
	firstName: 'Joe',
	lastName: 'Smith',
	age: '30',
	address": 'Bleaker St'
	phoneNumber: '333-2222',
	favoriteColor: 'green'
}
```

[//]: # (**Bonus**: You'll notice your prompts are "What is your firstName?" when you do this.)

[//]: # (Try to come up with a creative way to make your questions look more friendly, like)

[//]: # ("What is your first name?" while still retaining the firstName property in the output)

### 3. Generate a random list of numbers based on a user defined `length`, `min`, and `max` variables

Create a new function, `generateList(length, min, max)`, to generate a random array that will be the size of `length` and contain values between the `min` and `max` values.

Collect the parameters using user input so script execution looks like:
```
$ node ./part3.js
Random List Generator!
How long would you like your list to be? 10
What's the min list value? -5
What's the max list value? 20
[-2, 9, 5, 14, 10, -1, 5, 4,  4, -2]
```

### 4. Input validation

Run your program and input a non-numeric value for min or max. Your result is something like below:
```
$ node ./part3.js
Random List Generator!
How long would you like your list to be? 10
What the min list value? ten
What the max list value? sadfs
[
  NaN, NaN, NaN, NaN,
  NaN, NaN, NaN, NaN,
  NaN, NaN
]
```

Create a new function, `getNumericInput`, that will prompt the user for input until they provide a numeric value.

**Bonus**: Error Handling - Ensure that the `max` is always greater than the `min` value

### 5. Filter data

First, generate a list of 50 values with a `min` of -100 and `max` of 100 (manually or programmatically). Save this list in an array called `p5List`.

Next generate two new arrays from the starting list, one where `values >= 0` and one where `values < 0`.

Output the result of the new arrays.

**Hint**: You can use the `array.filter()` function to do this.

Expected Output:
```javascript
Values >= 0 in the list 80,47,93,12,57,32,40,15,59,51,58,54,68,24,70,95,87,21,17,14,94,69
Values < 0 in the list -11,-46,-49,-2,-82,-20,-96,-67,-82,-66,-1,-97,-35,-11,-50,-79,-23,-3,-61,-31,-59,-36,-14,-66,-12,-75,-88,-52
```

### 6. More array manipulation

Write a function to generate the following stats:
    1. number of values > 0
    2. number of values < 0
    3. number of zeros
    4. number of odd values
    5. number of even values

The output should be an object like the following example:
```javascript
{
  positive: [
    80, 47, 93, 12, 57, 32, 40,
    15, 59, 51, 58, 54, 68, 24,
    70, 95, 87, 21, 17, 14, 94,
    69
  ],
  negative: [
    -11, -46, -49,  -2, -82, -20,
    -96, -67, -82, -66,  -1, -97,
    -35, -11, -50, -79, -23,  -3,
    -61, -31, -59, -36, -14, -66,
    -12, -75, -88, -52
  ],
  zero: [],
  odd: [
    47, 93, 57, 15, 59,
    51, 95, 87, 21, 17,
    69
  ],
  even: [
     80, -46,  -2, -82, -20,  12, -96,
    -82, -66,  32,  40, -50,  58,  54,
     68,  24, -36,  70, -14, -66, -12,
    -88,  14,  94, -52
  ]
}
```

Now, take the original list and invert all numbers such that all positive numbers are now negative, and vice-versa.

**Hint**: Use `array.map()`. Mapping allows you to modify each of the values in an array and return a new array. Save the new list into a variable called `p5ListInverse`.

Then an object like before. Here's an example:
```js
{
  positive: [
    11, 46, 49,  2, 82, 20, 96, 67,
    82, 66,  1, 97, 35, 11, 50, 79,
    23,  3, 61, 31, 59, 36, 14, 66,
    12, 75, 88, 52
  ],
  negative: [
    -80, -47, -93, -12, -57,
    -32, -40, -15, -59, -51,
    -58, -54, -68, -24, -70,
    -95, -87, -21, -17, -14,
    -94, -69
  ],
  zero: [],
  odd: [
    11, 49, 67, 1, 97, 35,
    11, 79, 23, 3, 61, 31,
    59, 75
  ],
  even: [
    -80,  46,   2,  82, 20, -12,  96,
     82,  66, -32, -40, 50, -58, -54,
    -68, -24,  36, -70, 14,  66,  12,
     88, -14, -94,  52
  ]
}
```

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

## Part 4: Pushing to Gitlab

Once you completed all parts of the lab, you're ready to commit your progress
to Git!

First - Go to https://gitlab.com to create an account. You can use your personal
email if you don't have an account already.

Next, please provide your Gitlab username to the instructors, so they can give you
permissions to the repository.

Next, you'll make a new branch where you'll add your results

```shell
$ git branch <YOUR_NAME>
$ git checkout <YOUR_NAME>
```

Once you're in the new branch, go ahead and add your files

**Note**: Ensure you're adding files from lab 1

```shell
$ git add .
$ git commit -m "Adding files from lab 1"
```

Since this is a new branch, it is not connected to the upstream repo. Let's link the new branch to the repo. (This is a one time setup thing)
```shell
$ git push --set-upstream origin <YOUR_NAME>
```

Now we can push our commits
```shell
$ git push
```

Finally, call an instructor over to check that you've submitted your lab.

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

### Congrats! You've completed Lab 1!
