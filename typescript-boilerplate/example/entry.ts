import {CarHorn, HornStyles} from "./CarHorn.js";
import {Car} from "./Car.js";

//
// Typescript Example
//

// Pick a couple of car horns
const carHornA = new CarHorn(HornStyles.WIMPY);
const carHornB = new CarHorn(HornStyles.PRETTY_LOUD);

// Build a couple of cars
const carA = new Car({
    carHorn: carHornA,
    numOfWheels: 4,
    batteryChargingCapacity: 8600,
});
const carB = new Car({
    carHorn: carHornB,
    numOfWheels: 6,
    batteryChargingCapacity: 3700,
    motorBrand: "Supra Strong",
});

// Drive the cars
carA.drive("P Sherman, 42 Wallaby Way, Sydney");
carB.driveButTHISAlwaysWorks("The Cupboard under the Stairs, 4 Privet Drive, Little Whinging, Surrey");

console.log("All done with these cars, exiting!");