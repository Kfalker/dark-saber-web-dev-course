import json


with open("poker.txt", "r") as f:
	output = [l.replace("\n","") for l in  f.readlines()]
	with open("poker.json", "w") as out:
		json.dump(output, out)
