# Lab 3 - Poker Hand Calculator

##### Estimated Time to Complete: 2 hour - 3 hours

#### Instructions

In the card game poker, a hand consists of five cards and are ranked, from lowest to highest, in the following way:

**High Card**: Highest <u>value</u> card.<br>
**One Pair**: Two cards of the same <u>value</u>.<br>
**Two Pairs**: Two different pairs.<br>
**Three of a Kind**: Three cards of the same <u>value</u>.<br>
**Straight**: All cards are consecutive <u>value</u>s.<br>
**Flush**: All cards of the same <u>suit</u>.<br>
**Full House**: Three of a kind and a pair.<br>
**Four of a Kind**: Four cards of the same <u>value</u>.<br>
**Straight Flush**: All cards are consecutive <u>value</u>s of same <u>suit</u>.<br>
**Royal Flush**: Ten, Jack, Queen, King, Ace, in same <u>suit</u>.<br>

The cards are valued in the order:
2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace

If two players have the same ranked hands then the rank made up of the highest value wins; for example, a pair of eights beats a pair of fives (see example 1 below). But if two ranks tie, for example, both players have a pair of queens, then highest cards in each hand are compared (see example 4 below); if the highest cards tie then the next highest cards are compared, and so on

Consider the following five hands dealt to two players:

| Hand | Player 1       |                                   | Player 2       |                                    | Winner   |
| ---- | -------------- | --------------------------------- | -------------- | ---------------------------------- | -------- |
| 1    | 5H 5C 6S 7S KD | Pair of Fives                     | 2C 3S 8S 8D TD | Pair of Eights                     | Player 2 |
| 2    | 5D 8C 9S JS AC | Highest card Ace                  | 2C 5C 7D 8S QH | Highest card Queen                 | Player 1 |
| 3    | 2D 9C AS AH AC | Three Aces                        | 3D 6D 7D TD QD | Flush with Diamonds                | Player 2 |
| 4    | 4D 6S 9H QH QC | Pair of Queens, Highest card Nine | 3D 6D 7H QD QS | Pair of Queens, Highest card Seven | Player 1 |
| 5    | 2H 2D 4C 4D 4S | Full House With Three Fours       | 3C 3D 3S 9S 9D | Full House with Three Threes       | Player 1 |

The file, poker.txt, contains one-thousand random hands dealt to two players. Each line of the file contains ten cards (each card is separated by a single space): the first five are Player 1's cards and the last five are Player 2's cards. You can assume that all hands are valid (no invalid characters or repeated cards), each player's hand is in no specific order, and in each hand there is a clear winner.

Since parsing txt files in JavaScript is messy, we've taken the liberty to convert the .txt file to JSON for you! Please start with the poker.json file

How many hands does Player 1 win?

### Expectations

1. You must use JavaScript to create your solution
2. We expect you to use the NodeJS interpreter in order to run your program
3. The output must look like this "`Player 1 won XX hands and player 2 won XX.`" with `XX` being the number of hands won by the respective player

### Submission

When you believe you have the answer, call an instructor over to review your work

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

### Part 1: Pushing to Gitlab

Once you completed all parts of the lab, and you have a verified working solution, you're ready to commit your progress to Git!

Ensure you're using your project branch:
```shell
$ git branch
  master
* my-cool-branch
```

Commit your code:
```shell
$ git add .
$ git commit -m "Adding files from this lab"
$ git push

# alternatively
$ git commit -a -m "<my commit message>"
$ git push
```

Let us know when you've pushed your code to GitLab

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

### Congrats! You've completed Lab 3!
