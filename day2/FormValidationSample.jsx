import * as React from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";

export default function ValidationTextFields() {
  const [inError, setInError] = React.useState(false);
  const [input, setInput] = React.useState("");

  const validateInput = (val) => {
    setInput(val); // Set the Input
    if (val === "" || val === undefined || (val > 0 && val <= 10)) {
      setInError(false);
    } else {
      setInError(true);
    }
  };

  return (
    <Box
      component="form"
      sx={{
        "& .MuiTextField-root": { m: 1, width: "25ch" },
      }}
      noValidate
      autoComplete="off"
    >
      <div>
        <TextField
          error={inError}
          id="outlined-error-helper-text"
          label="Demo Input Validation"
          value={input}
          onChange={(e) => validateInput(e.target.value)}
          helperText={
            inError ? "Incorrect entry, must be number between 1-10" : " "
          }
        />
      </div>
    </Box>
  );
}
