# Lab 2 - Day of the Week Generator

##### Estimated Time to Complete: 1 hour - 1.5 hours

In this lab, we will put together what you've learned so far in order
to make a program that generate the day of the week for any given date (AD calendar).

Your program will accept a month, day, and year and must produce the day of the week.

For example
```
What is the month (1 to 12)? 1
What is the day (1 to 31)? 30
What is the year (e.g., 2007) 2007
Total Days Past 766675
Your date falls on a Wednesday
```

Your program must ensure to account for incorrect input. Specifically, it must
gracefully handle situations where the input is out of valid ranges.

For incorrect values, you should print a message similar to the following:
```
What is the month (1 to 12)? 123
The month 123 is invalid. Try again
What is the month (1 to 12)? 1
What is the day (1 to 31)? 30
What is the year (e.g., 2007) 2007
Total Days Past 766675
Your date falls on a Wednesday
```

In addition, you must take into account leap years. The rules of leap years
were developed by the Romans where the following conditions need to be met:

1. Every 4 years is a leap year
2. Every 100 years a leap year is skipped unless the year is divisible by 400

**Note**: You **CANNOT** use any `JavaScript` `datetime` libraries to develop your solution.

## Part 0: Create the File

Create a file named `DayOfTheWeek.js` where you'll write your program

## Part 1: Build the Program

### Solution Requirements

Your solution must be broken into multiple functions to help you solve the
solution. Do not write one big mega-function (that would be mega-not-cool).

### Suggestions

- Try to use `[].includes()` in your solution, this will be helpful for parsing input.
- Try to use a `switch` statement in your solution
- Break out the problem into functions
- **Suggested functions**

  1. A function that determines if a given year is a leap year
  2. A function that computes the number of days that have past
  3. A function that takes the number of days that have past and solves the day of the week challenge

Once done, call us over to verify your solution.

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

### Part 2: Pushing to Gitlab

Once you completed all parts of the lab, and you have a verified working solution,
you're ready to commit your progress to Git!

Ensure you're using your project branch. You should be on the same branch that
you created in the last lab
```shell
$ git branch
  master
* my-cool-branch
```

Commit your code:
```shell
$ git add .
$ git commit -m "Adding files from this lab"
$ git push

# alternatively
$ git commit -a -m "<my commit message>"
$ git push
```

Let us know when you've pushed your code to GitLab

Signature \***\*\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\*\***

### Congrats! You've completed Lab 2!
