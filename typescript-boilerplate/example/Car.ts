import {CarHorn} from "./CarHorn.js";

interface CarConfiguration {
    numOfWheels: number,
    motorBrand?: string,
    batteryChargingCapacity: number | null,
    carHorn: CarHorn

}

export class Car {

    private carConfig: CarConfiguration;

    constructor(configuration: CarConfiguration) {
        this.carConfig = configuration;
    }

    drive(address: string) {
        console.log(`Driving to ${address}`);
    }

    driveButTHISAlwaysWorks = (address: string) => {
        console.log(`Driving to ${address}, not worried if "this" is pointing to the correct context.`);
    }
}