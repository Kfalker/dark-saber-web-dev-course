# Dark Saber Web Development Course

## This class was built to get airmen familiar with modern web dev technologies

## Core Topics

- JS
- ReactJS
- NodeJS
- ExpressJS
- Docker
- Git / GitLab
- AWS
- MongoDB / DynamoDB
- Bash
