import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h3>Dark Saber Web Course - Lab 6</h3>
        <h4>Poker Hands</h4>
        <h5>See README.md for instructions :)</h5>
        <a
          className="App-link"
          href="https://gitlab.com/Collen-Roller/dark-saber-web-dev-course/-/tree/main/day2/lab6"
          target="_blank"
          rel="noopener noreferrer"
        >
          Click here to get started!
        </a>
      </header>
    </div>
  );
}

export default App;
